package com.hash.kmmrequestlibs
import android.os.Handler
import android.os.Looper
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.beust.klaxon.Klaxon
import com.github.kittinunf.fuel.*
import com.github.kittinunf.result.Result
import java.lang.reflect.Type
import java.util.*

actual class Fetch actual constructor(){
    actual val fetch: String = "Android ${android.os.Build.VERSION.SDK_INT} KMM"

    actual fun getString (): String {
        return "from function"
    }

    actual suspend fun getLibs() : libsd? {
        val serverurl = "https://appserver1.mdrm.hash.id"
        val (request, response, result) = Fuel.post("${serverurl}/api/entity/get-library").responseString()
        when (result) {
            is Result.Failure -> {
                val ex = result.getException()
                println(ex)
                println(String(response.data))
                println(request)
                return null
            }
            is Result.Success -> {
                val data = result.get()
                println("result getapp : $data")
                val result = Klaxon()
                    .parse<LibsData>(data)
                println(result?.count)
                println(result?.data)
//                return "Libs Count : " + result?.count.toString()
                return result
            }
        }
    }

}