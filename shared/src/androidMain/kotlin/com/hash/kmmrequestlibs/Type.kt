package com.hash.kmmrequestlibs

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

data class LibsData (
    @SerializedName("count" ) var count : Int?            = null,
    @SerializedName("data"  ) var data  : ArrayList<Data> = arrayListOf()
)


data class Data (
    @SerializedName("id"         ) var id        : String? = null,
    @SerializedName("created_at" ) var createdAt : String? = null,
    @SerializedName("updated_at" ) var updatedAt : String? = null,
    @SerializedName("deleted_at" ) var deletedAt : String? = null,
    @SerializedName("label"      ) var label     : String? = null,
    @SerializedName("metadata"   ) var metadata  : String? = null
)

actual typealias libsd = LibsData
