package com.hash.kmmrequestlibs

expect class Fetch() {
    val fetch: String
    fun getString(): String
    suspend fun getLibs(): libsd?
}