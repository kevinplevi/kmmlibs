package com.hash.kmmrequestlibs

class Greeting {
    fun greeting(): String {
        return "Hello, ${Platform().platform}!"
    }

    fun fetching(): String {
        return "Hello, ${Fetch().fetch} , ${Fetch().getString()}!"
    }

    suspend fun getLibs(): libsd? {
        return Fetch().getLibs()
    }
}