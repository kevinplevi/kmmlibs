package com.hash.kmmrequestlibs.android

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.hash.kmmrequestlibs.Greeting
import android.widget.TextView
import kotlinx.coroutines.*

fun greet(): String {
    return Greeting().fetching()
}

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tv: TextView = findViewById(R.id.text_view)
        tv.text = greet()

        GlobalScope.launch(Dispatchers.IO) {
            runBlocking {
                val a = async { Greeting().getLibs() }
                val result = a.await()
                var count = "COUNT : " + result?.count.toString() + "\nFIRST ITEM : " + result?.data?.get(0)?.label
                runOnUiThread {
                    tv.text = count
                }
            }
        }
    }
}
